use std::env;
use std::fs::File;
use std::io::Read;
use std::process;

use omega_i2c_exp::oled;

fn init() {
    oled::check_init().expect("Check init");

    let mut display = oled::Display::new();

    display.driver_init().expect("Display driver init");
}

fn clear() {
    oled::check_init().expect("Check init");

    oled::clear().expect("Clear display");
}

fn power(argument: &str) {
    let power_on = match_on_off(argument);

    oled::check_init().expect("Check init");

    oled::set_display_power(power_on).expect("Set display power")
}

fn invert(argument: &str) {
    let invert = match_on_off(argument);

    oled::check_init().expect("Check init");

    let mode = if invert {
        oled::DisplayMode::Inverted
    } else {
        oled::DisplayMode::Normal
    };

    oled::set_display_mode(mode).expect("Set display mode")
}

fn dim(argument: &str) {
    let dim = match_on_off(argument);

    oled::check_init().expect("Check init");

    let display = oled::Display::new();

    display.set_dim(dim).expect("Set dim")
}

fn cursor(argument1: &str, argument2: &str) {
    let row = u8::from_str_radix(argument1, 10).expect("Parse row value");

    let column = u8::from_str_radix(argument2, 10).expect("Parse column value");

    log::info!("Setting cursor to ({}, {})", row, column);

    let mut display = oled::Display::new();

    display.set_text_columns().expect("Set text columns");

    oled::set_cursor(row, column).expect("Set cursor");
}

fn cursor_pixel(argument1: &str, argument2: &str) {
    let row = u8::from_str_radix(argument1, 10).expect("Parse row value");

    let pixel = u8::from_str_radix(argument2, 10).expect("Parse pixel value");

    log::info!("Setting cursor to row: {}, pixel: {}", row, pixel);

    let mut display = oled::Display::new();

    display.set_image_columns().expect("Set image columns");

    oled::set_cursor_by_pixel(row, pixel).expect("Set cursor by pixel");
}

fn write(argument: &str) {
    oled::check_init().expect("Check init");

    let mut display = oled::Display::new();

    display.write(argument).expect("Write message")
}

fn write_byte(argument: &str) {
    let value = u8::from_str_radix(argument, 16).expect("Parse byte value");

    oled::check_init().expect("Check init");

    let display = oled::Display::new();

    display.write_byte(value).expect("Write byte")
}

fn draw(argument: &str) {
    let size = oled::WIDTH as usize * oled::HEIGHT as usize / 8;

    let mut buffer = vec![0_u8; size];

    let mut file = File::open(argument).expect("Open image file");

    file.read_to_end(&mut buffer).expect("Read image file");

    let mut display = oled::Display::new();

    display.draw(&buffer).expect("Draw image");
}

fn scroll(argument: &str) {
    oled::check_init().expect("Check init");

    let (vertical, val1) = match argument {
        "left" => {
            (
                false, // horizontal scrolling
                0,
            ) // scrolling left
        }
        "right" => {
            (
                false, // horizontal scrolling
                1,
            ) // scrolling right
        }
        "diagonal-left" => {
            (
                true, // vertical scrolling
                0,
            ) // scrolling up
        }
        "diagonal-right" => {
            (
                true, // vertical scrolling
                1,
            ) // scrolling down
        }
        _ => {
            oled::scroll_stop().expect("Scroll stop");
            return;
        }
    };

    if vertical {
        // diagonal scrolling
        oled::scroll_diagonal(
            val1,                  // direction
            oled::ScrollSpeed::F5, // scroll speed
            0,                     // # fixed rows
            oled::HEIGHT,          // # scrolling rows
            1,                     // rows to scroll by
            0,                     // horizontal start page
            oled::CHAR_ROWS - 1,   // horizontal end page
        )
        .expect("Diagonal scrolling");
    } else {
        // horizontal scrolling
        oled::scroll(val1, oled::ScrollSpeed::F5, 0, oled::CHAR_ROWS - 1)
            .expect("Horizontal scrolling");
    }
}

#[inline]
fn match_on_off(argument: &str) -> bool {
    match argument {
        "on" => true,
        "off" => false,
        _ => panic!("argument must be on/off"),
    }
}

fn print_usage() {
    println!("usage: omega-oled <command> <arguments>");
}

fn main() {
    let args: Vec<String> = env::args().skip(1).collect();

    if args.len() == 0 || args.len() > 3 {
        print_usage();
        process::exit(1);
    }

    pretty_env_logger::init();

    let command = &args[0];

    match command.as_str() {
        "init" => init(),
        "clear" => clear(),
        "power" => power(&args[1]),
        "invert" => invert(&args[1]),
        "write" => write(&args[1]),
        "write_byte" => write_byte(&args[1]),
        "dim" => dim(&args[1]),
        "cursor" => cursor(&args[1], &args[2]),
        "cursor_pixel" => cursor_pixel(&args[1], &args[2]),
        "scroll" => scroll(&args[1]),
        "draw" => draw(&args[1]),
        _ => panic!("Unsupported command: {}", command),
    }
}
