use std::env;
use std::process;

use omega_i2c_exp::mcp;
use omega_i2c_exp::relay;

fn init(address: u16) {
    let initialized = relay::check_init(address).expect("Initialize relay driver");

    if !initialized {
        relay::driver_init(address).expect("Initialize relay driver");
    }

    println!("Relay driver is initialized");
}

fn read(address: u16, channel: u8) {
    let state = relay::read_channel(address, channel).expect("Read channel state");

    println!("State: {:?}", state);
}

fn write(address: u16, channel: u8, state: u8) {
    relay::set_channel(address, channel, state).expect("Write channel state");

    println!("Relay state written");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("usage: omega-relay <command> <arguments>");
        process::exit(1);
    }

    pretty_env_logger::init();

    let command = &args[1];

    let mut channel = 0;

    if args.len() >= 3 {
        channel = u8::from_str_radix(&args[2], 10).unwrap();
    }

    let mut state = 0;

    if args.len() == 4 {
        state = u8::from_str_radix(&args[3], 10).unwrap();
    }

    // Functions expect an offset from the default 0x20 address
    let address = relay::RELAY_EXP_ADDR_DEFAULT - mcp::MCP23008_I2C_DEVICE_ADDR;

    match command.as_str() {
        "init" => init(address),
        "read" => read(address, channel),
        "write" => write(address, channel, state),
        _ => panic!("Unsupported command: {}", command),
    }
}
