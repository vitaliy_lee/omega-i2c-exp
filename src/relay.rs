use crate::mcp;
use crate::Result;

pub const RELAY_EXP_ADDR_DEFAULT: u16 = 0x27;
const RELAY_EXP_CHANNEL0: u8 = 0;
const RELAY_EXP_CHANNEL1: u8 = 1;

// read GPIO value, ie read the relay state
pub fn read_channel(address: u16, channel: u8) -> Result<u8> {
    // read the relay state
    let state = mcp::get_gpio(address, channel)?;

    log::debug!("Reading RELAY{} state: {}", channel, state);

    return Ok(state);
}

// set value to both GPIO0 and GPIO1 (both relay states)
pub fn set_all_channels(address: u16, state: u8) -> Result<()> {
    log::info!("Setting both RELAYS to {}", state);

    let value =
	// translate state to register value
	if state == 0 {
		0x0
	} else {
		0x3	// enable GPIO0 and GPIO1
	};

    // set the all relay channels to the specified state
    mcp::set_all_gpio(address, value)
}

// perform basic initialization of GPIO chip
pub fn driver_init(address: u16) -> Result<()> {
    // onionPrint(ONION_SEVERITY_INFO, "> Initializing Relay Expansion chip\n");

    // set all GPIOs to output
    mcp::set_all_direction(address, 0)?;
    // if (status == EXIT_FAILURE) {
    // 	onionPrint(ONION_SEVERITY_FATAL, "relay-exp:init:: Setting output direction failed\n");
    // 	return status;
    // }

    // disable all pullup resistors
    mcp::set_all_pullup(address, 0)?;
    // if (status == EXIT_FAILURE) {
    // 	onionPrint(ONION_SEVERITY_FATAL, "relay-exp:init:: Disabling pullup resistors failed\n");
    // 	return status;
    // }

    // set all GPIOs to 0
    mcp::set_all_gpio(address, 0)
    // if (status == EXIT_FAILURE) {
    // 	onionPrint(ONION_SEVERITY_FATAL, "relay-exp:init:: Reseting GPIOs failed\n");
    // 	return status;
    // }
}

// check if GPIO0 and GPIO1 are properly initialized
pub fn check_init(address: u16) -> Result<bool> {
    // set default to not initialized
    let mut initialized = false;

    // find GPIO0 direction
    let gpio0_direction = mcp::get_direction(address, RELAY_EXP_CHANNEL0)?;

    // find GPIO1 direction
    let gpio1_direction = mcp::get_direction(address, RELAY_EXP_CHANNEL1)?;

    // find status of GPIO0's pull-up resistor
    let gpio0_pullup = mcp::get_input_polarity(address, RELAY_EXP_CHANNEL0)?;

    // find status of GPIO1's pull-up resistor
    let gpio1_pullup = mcp::get_input_polarity(address, RELAY_EXP_CHANNEL1)?;

    // check for any initialization
    if gpio0_direction == 0 && gpio1_direction == 0 && gpio0_pullup == 0 && gpio1_pullup == 0 {
        initialized = true;
    }

    return Ok(initialized);
}

// set GPIO value - change the relay state
pub fn set_channel(address: u16, channel: u8, state: u8) -> Result<()> {
    // set the relay channel to the specified state
    log::debug!("Setting RELAY{} to {}", channel, state);

    mcp::set_gpio(address, channel, state)
}
