use std::error;
use std::result;

mod errors;
pub mod i2c;
pub mod mcp;

#[cfg(feature = "relay")]
pub mod relay;

#[cfg(feature = "oled")]
pub mod oled;

#[cfg(feature = "pwm")]
pub mod pwm;

pub type Result<T> = result::Result<T, Box<dyn error::Error>>;
