use crate::errors::ApplicationError;
use crate::i2c;
use crate::Result;

const DEVICE_NUM: u8 = i2c::DEFAULT_ADAPTER;
const DEVICE_ADDR: u16 = 0x5a;

const REG_OFFSET_BYTE0: u8 = 0x0;
const REG_OFFSET_BYTE1: u8 = 0x1;

const REG_OFFSET_ON_BYTES: u8 = 0x0;
const REG_OFFSET_OFF_BYTES: u8 = 0x2;

const NUM_CHANNELS: u8 = 16;

const OSCILLATOR_CLOCK: f64 = 25000000.0;
const PULSE_TOTAL_COUNT: u16 = 4096;

const PRESCALE_MIN_VALUE: u8 = 0x03;
const PRESCALE_MAX_VALUE: u8 = 0xff;

const LED_FULL_VAL: u16 = 0x1000;

const REG_ADDR_DRIVER0: u8 = 0x06;
const REG_ADDR_DRIVER1: u8 = 0x0a;
const REG_ADDR_DRIVER2: u8 = 0x0e;
const REG_ADDR_DRIVER3: u8 = 0x12;
const REG_ADDR_DRIVER4: u8 = 0x16;
const REG_ADDR_DRIVER5: u8 = 0x1a;
const REG_ADDR_DRIVER6: u8 = 0x1e;
const REG_ADDR_DRIVER7: u8 = 0x22;
const REG_ADDR_DRIVER8: u8 = 0x26;
const REG_ADDR_DRIVER9: u8 = 0x2a;
const REG_ADDR_DRIVER10: u8 = 0x2e;
const REG_ADDR_DRIVER11: u8 = 0x32;
const REG_ADDR_DRIVER12: u8 = 0x36;
const REG_ADDR_DRIVER13: u8 = 0x3a;
const REG_ADDR_DRIVER14: u8 = 0x3e;
const REG_ADDR_DRIVER15: u8 = 0x42;
const REG_ADDR_DRIVER_ALL: u8 = 0xfa;

// define array of register offsets
const DRIVER_ADDR: [u8; NUM_CHANNELS as usize] = [
    REG_ADDR_DRIVER0,
    REG_ADDR_DRIVER1,
    REG_ADDR_DRIVER2,
    REG_ADDR_DRIVER3,
    REG_ADDR_DRIVER4,
    REG_ADDR_DRIVER5,
    REG_ADDR_DRIVER6,
    REG_ADDR_DRIVER7,
    REG_ADDR_DRIVER8,
    REG_ADDR_DRIVER9,
    REG_ADDR_DRIVER10,
    REG_ADDR_DRIVER11,
    REG_ADDR_DRIVER12,
    REG_ADDR_DRIVER13,
    REG_ADDR_DRIVER14,
    REG_ADDR_DRIVER15,
];

const REG_MODE1: u8 = 0x00;
const REG_MODE2: u8 = 0x01;
const REG_ADDR_PRESCALE: u8 = 0xfe;

const REG_MODE1_RESET: u8 = 0x80;
const REG_MODE1_SLEEP: u8 = 0x10;
const REG_MODE1_ALLCALL: u8 = 0x01;
const REG_MODE2_OUTDRV: u8 = 0x04;

pub const FREQUENCY_DEFAULT: f64 = 50.0;

// check if the oscillator is running
pub fn check_init() -> Result<bool> {
    // check if oscillator is in sleep mode
    let sleep_mode = get_sleep_mode()?;

    // check if oscillator is in sleep mode
    let initialized = !sleep_mode;

    log::debug!(
        "sleep mode is {}, initialized is {}",
        sleep_mode,
        initialized
    );

    Ok(initialized)
}

// disable the chip - set oscillator to sleep
pub fn disable_chip() -> Result<()> {
    log::info!("Oscillator going into sleep mode, PWM disabled");

    // enable oscillator sleep mode
    set_sleep_mode(true)
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:pwmSetFreq:: disabling SLEEP mode failed\n");
}

// program the prescale value for desired pwm frequency
pub fn set_frequency(freq: f64) -> Result<()> {
    //// calculate the prescale value
    // prescale = round( osc_clk / pulse_count x update_rate ) - 1
    let mut prescale = (OSCILLATOR_CLOCK / PULSE_TOTAL_COUNT as f64 * freq).round() as u8 - 1;

    // clamp the value
    if prescale < PRESCALE_MIN_VALUE {
        prescale = PRESCALE_MIN_VALUE;
    } else if prescale > PRESCALE_MAX_VALUE {
        prescale = PRESCALE_MAX_VALUE;
    }

    // read current prescale value
    let mut value = i2c::read_byte(DEVICE_NUM, DEVICE_ADDR, REG_ADDR_PRESCALE)?;
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:pwmSetFreq:: read PRESCALE failed\n");

    // only program frequency if new frequency is required
    if prescale != value {
        log::info!(
            "Setting signal frequency to {:0>.2} Hz (prescale: 0x{:02x})",
            freq,
            prescale
        );

        //// Go to sleep
        // read MODE1 register
        value = i2c::read_byte(DEVICE_NUM, DEVICE_ADDR, REG_MODE1)?;
        // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:pwmSetFreq:: read MODE1 failed\n");

        // enable sleep mode to disable the oscillator
        set_sleep_mode(true)?;
        // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:pwmSetFreq:: disabling SLEEP mode failed\n");

        // set the prescale value
        i2c::write(DEVICE_NUM, DEVICE_ADDR, REG_ADDR_PRESCALE, prescale)?;
        // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:pwmSetFreq:: setting prescale value failed\n");

        //// wake up - reset sleep
        // disable sleep mode to enable the oscillator
        set_sleep_mode(false)?;
        // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:pwmSetFreq:: disabling SLEEP mode failed\n");

        // reset
        set_reset()?;
        // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:pwmSetFreq:: reset failed\n");
    }

    Ok(())
}

// run the initial oscillator setup
pub fn driver_init() -> Result<()> {
    log::info!("Initializing PWM Expansion chip");

    // set all channels to 0
    setup_driver(0, 0.0, 0.0)?;

    // set PWM drivers to totem pole
    let mut address = REG_MODE2;
    let mut value = REG_MODE2_OUTDRV & 0xff;
    i2c::write(DEVICE_NUM, DEVICE_ADDR, address, value)?;
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:init:: write to MODE2 failed\n");

    // enable all call
    address = REG_MODE1;
    value = REG_MODE1_ALLCALL & 0xff;
    i2c::write(DEVICE_NUM, DEVICE_ADDR, address, value)?;
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:init:: write to MODE2 failed\n");

    // wait for oscillator
    // usleep(1000);

    //// reset MODE1 (without sleep)
    // disable SLEEP mode
    set_sleep_mode(false)?;
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:init:: disabling SLEEP mode failed\n");

    // enable the reset
    set_reset()
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:init:: reset failed\n");
}

// i2c register writes to set sleep mode
fn set_sleep_mode(sleep_mode: bool) -> Result<()> {
    // read MODE1 register
    let address = REG_MODE1;
    let mut value = i2c::read_byte(DEVICE_NUM, DEVICE_ADDR, address)?;
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:_pwmSetSleepMode:: read MODE1 failed\n");

    // set desired sleep mode
    if sleep_mode {
        // enable SLEEP mode
        value |= REG_MODE1_SLEEP;
    } else {
        // disable SLEEP mode
        value &= !REG_MODE1_SLEEP;
    }

    // write to MODE1 register
    i2c::write(DEVICE_NUM, DEVICE_ADDR, address, value).map(|_| ())

    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:_pwmSetSleepMode:: write to MODE1 failed\n");

    // wait for the oscillator
    // usleep(1000);
}

// i2c register writes to set sw reset
fn set_reset() -> Result<()> {
    // read MODE1 register
    let address = REG_MODE1;
    let mut value = i2c::read_byte(DEVICE_NUM, DEVICE_ADDR, address)?;
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:_pwmSetReset:: read MODE1 register failed\n");

    // enable reset
    value |= REG_MODE1_RESET;
    i2c::write(DEVICE_NUM, DEVICE_ADDR, address, value).map(|_| ())

    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:_pwmSetReset:: write to MODE1 register failed\n");

    // wait for the oscillator
    // usleep(1000);
}

// perform PWM driver setup based on duty and delay
pub fn setup_driver(driver_num: u8, duty: f64, delay: f64) -> Result<()> {
    // initialize the setup structure
    let setup = PWMSetup::new(driver_num, duty, delay)?;

    log::info!(
        "Generating PWM signal with {:0>.2}% duty cycle ({:0>.2}% delay)",
        duty,
        delay
    );
    //printf("PWM: start: %d (0x%04x), stop: %d (0x%04x)\n\n", setup.timeStart, setup.timeStart, setup.timeEnd, setup.timeEnd);

    // write on and off times via i2c
    setup.set_time()
}

struct PWMSetup {
    driver_num: u8,
    reg_offset: u8,
    time_start: u16,
    time_end: u16,
    prescale: u8,
}

impl PWMSetup {
    // initialize the pwm info struct
    fn new(driver_num: u8, duty: f64, delay: f64) -> Result<PWMSetup> {
        // find driver number and then register offset
        let reg_offset = get_driver_register_offset(driver_num)?;

        // find on and off times
        let (time_start, time_end) = pwm_calculate(duty, delay);

        let setup = PWMSetup {
            driver_num: 0,
            reg_offset: reg_offset,
            time_start: time_start,
            time_end: time_end,
            prescale: 0,
        };

        Ok(setup)
    }

    // write ON and OFF time values
    fn set_time(&self) -> Result<()> {
        // set the ON time
        write_value(self.reg_offset + REG_OFFSET_ON_BYTES, self.time_start)?;

        // set the OFF time
        write_value(self.reg_offset + REG_OFFSET_OFF_BYTES, self.time_end)
    }
}

// return register offset for specified driver/channel
fn get_driver_register_offset(driver_num: u8) -> Result<u8> {
    // find the address
    if driver_num > NUM_CHANNELS {
        // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:: invalid driver selection, %d\n", driverNum);
        return Err(ApplicationError::new("invalid driver selection"));
    }

    let address = DRIVER_ADDR[driver_num as usize];
    return Ok(address);
}

// calculate the ON and OFF time values
fn pwm_calculate(duty: f64, delay: f64) -> (u16, u16) {
    // to do: add case for 0 duty / 100 duty,
    // set the full OFF / full ON bits

    if duty == 100.0 {
        // set LEDs to FULL_ON
        (LED_FULL_VAL, 0)
    } else if duty == 0.0 {
        // set LEDs to FULL_OFF
        (0, LED_FULL_VAL)
    } else {
        // find duty and delay in terms of numbers
        let count_on = duty_to_count(duty);
        let count_delay = duty_to_count(delay);

        let (time_start, mut time_end) =
		// calculate the time to assert and deassert the signal
		if count_delay > 0 {
			// with delayed start
			(count_delay - 1, count_delay - 1 + count_on)
		} else {
			// no delay - start at 0
			(0, count_on - 1)
		};

        // take care of the case where delay + duty are more than 100
        if time_end > PULSE_TOTAL_COUNT {
            time_end -= PULSE_TOTAL_COUNT;
        }

        (time_start, time_end)
    }
}

// split value into two bytes, and write to the L and H registers (via I2C)
fn write_value(address: u8, value: u16) -> Result<()> {
    // write first byte to L
    let mut wr_address = address + REG_OFFSET_BYTE0;
    let mut byte = (value & 0xff) as u8;

    //printf("Writing to addr: 0x%02x, data: 0x%02x\n", wr_address, byte);
    i2c::write(DEVICE_NUM, DEVICE_ADDR, wr_address, byte)?;

    // write second byte to H
    wr_address = address + REG_OFFSET_BYTE1;
    byte = ((value >> 8) & 0xff) as u8;

    //printf("Writing to addr: 0x%02x, data: 0x%02x\n", wr_address, byte);
    i2c::write(DEVICE_NUM, DEVICE_ADDR, wr_address, byte).map(|_| ())
}

// find time count based on duty percentage (0-100)
// 	return value: the count
fn duty_to_count(duty: f64) -> u16 {
    let duty_decimal = duty / 100.0;

    // convert duty to count
    (duty_decimal * PULSE_TOTAL_COUNT as f64).round() as u16
}

// i2c register read to find if chip (oscillator) is in sleep mode
fn get_sleep_mode() -> Result<bool> {
    // read MODE1 register
    let value = i2c::read_byte(DEVICE_NUM, DEVICE_ADDR, REG_MODE1)?;
    // onionPrint(ONION_SEVERITY_FATAL, "pwm-exp:pwmCheckInit:: read MODE1 failed\n");

    // check if oscillator is in sleep mode
    if (value & REG_MODE1_SLEEP) == REG_MODE1_SLEEP {
        Ok(true)
    } else {
        Ok(false)
    }
}
