use std::result;

use i2cdev::core::*;
use i2cdev::linux::{LinuxI2CDevice, LinuxI2CError, LinuxI2CMessage};

use crate::Result;

pub const DEFAULT_ADAPTER: u8 = 0;

pub fn read_byte(device_num: u8, device_address: u16, address: u8) -> Result<u8> {
    log::debug!(
        "Reading byte from device {:#x}: addr = {:#x}",
        device_address,
        address
    );

    let mut device = open_device(device_num, device_address)?;

    let mut address_buffer = [0u8; 1];

    // push the address and data values into the buffer
    address_buffer[0] = address;

    let mut value_buffer = [0u8; 1];

    let mut messages = [
        LinuxI2CMessage::write(&mut address_buffer),
        LinuxI2CMessage::read(&mut value_buffer),
    ];

    device.transfer(&mut messages)?;

    let value = value_buffer[0];

    log::debug!("\tread byte, value: {:#x}", value);

    Ok(value)
}

pub fn write(device_num: u8, device_address: u16, address: u8, value: u8) -> Result<u32> {
    log::debug!(
        "Writing to device {:#x}: addr = {:#x}, data = {:#x}",
        device_address,
        address,
        value
    );

    let mut device = open_device(device_num, device_address)?;

    let mut buffer = [0u8; 2];

    // push the address and data values into the buffer
    buffer[0] = address;
    buffer[1] = value;

    let mut messages = [LinuxI2CMessage::write(&mut buffer)];

    match device.transfer(&mut messages) {
        Ok(num) => Ok(num),
        Err(error) => Err(Box::new(error)),
    }
}

fn open_device(
    device_num: u8,
    device_address: u16,
) -> result::Result<LinuxI2CDevice, LinuxI2CError> {
    let device_path = format!("/dev/i2c-{}", device_num);
    LinuxI2CDevice::new(device_path, device_address)
}
