use std::error;
use std::fmt;

// Define our error types. These may be customized for our error handling cases.
// Now we will be able to write our own errors, defer to an underlying error
// implementation, or do something in between.
#[derive(Debug, Clone)]
pub struct ApplicationError {
    reason: String,
}

impl ApplicationError {
    pub fn new(reason: &str) -> Box<ApplicationError> {
        let error = ApplicationError {
            reason: reason.to_string(),
        };

        return Box::new(error);
    }
}

// Generation of an error is completely separate from how it is displayed.
// There's no need to be concerned about cluttering complex logic with the display style.
//
// Note that we don't store any extra info about the errors. This means we can't state
// which string failed to parse without modifying our types to carry that information.
impl fmt::Display for ApplicationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.reason)
    }
}

// This is important for other errors to wrap this one.
impl error::Error for ApplicationError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        // Generic error, underlying cause isn't tracked.
        None
    }
}

// #[derive(Debug)]
// pub enum ApplicationError {
//     /// Some unspecified error.
//     // Any(Box<dyn StdError + Send + Sync + 'static>),
//     // UnknownError,
//     // UnreadableMessage,
//     InvalidGPIO {
//         gpio: u8,
//     },
//     // RequestError {
//     //     description: String,
//     // },
//     // ParsingError {
//     //     description: String,
//     // },
//     // MissingChatId,
//     // NoInput,
// }

// impl ApplicationError {
//     /// Prints error to stderr and exits the program.
//     pub fn exit(self) {
//         eprintln!("{}", self);
//         std::process::exit(1);
//     }
// }

// impl<'a> fmt::Display for ApplicationError {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         match self {
//             // Error::RequestError { ref description } => {
//             //     write!(f, "\nMessage failed to send due to:\n\t{}", description)
//             // }
//             ApplicationError::InvalidGPIO { ref gpio } => {
//                 write!(f, "\nCould not find file in gpio:\n\t{}", gpio)
//             }
//             // Error::MissingChatId => {
//             //     write!(f, "\nChat ID not found in flags or TEPE_TELEGRAM_CHAT_ID")
//             // }
//             // Error::NoInput => write!(f, "\nNo input was given"),
//             // Error::ParsingError { ref description } => {
//             //     write!(f, "\nError from parsing:\n\t{}", description)
//             // }
//             // Error::UnreadableMessage => write!(f, "\nIssue parsing message"),
//             _ => write!(f, "\nTODO: add error description"),
//         }
//     }
// }
