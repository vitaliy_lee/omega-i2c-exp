use crate::errors;
use crate::i2c;
use crate::Result;

const MCP23008_REG_GPIO: u8 = 0x09;
const MCP23008_NUM_GPIOS: u8 = 8;
const MCP23008_I2C_DEVICE_NUM: u8 = i2c::DEFAULT_ADAPTER;
pub const MCP23008_I2C_DEVICE_ADDR: u16 = 0x20;
const MCP23008_REG_IODIR: u8 = 0x00;
const MCP23008_REG_GPPU: u8 = 0x06;
const MCP23008_REG_IPOL: u8 = 0x01;

// validate gpio number
fn is_gpio_valid(gpio: u8) -> bool {
    if gpio < MCP23008_NUM_GPIOS {
        return true;
    }

    return false;
}

pub fn get_gpio(device_address: u16, gpio: u8) -> Result<u8> {
    get_attribute(device_address, gpio, MCP23008_REG_GPIO)
}

pub fn set_gpio(device_address: u16, gpio: u8, value: u8) -> Result<()> {
    set_attribute(device_address, gpio, MCP23008_REG_GPIO, value)
}

// read register value
fn read_register(device_address: u16, address: u8) -> Result<u8> {
    i2c::read_byte(
        MCP23008_I2C_DEVICE_NUM,
        MCP23008_I2C_DEVICE_ADDR + device_address,
        address,
    )
}

// function to generalize attribute reading functions
fn get_attribute(device_address: u16, gpio: u8, address: u8) -> Result<u8> {
    // validate the gpio number
    if !is_gpio_valid(gpio) {
        return Err(errors::ApplicationError::new("invalid GPIO"));
    }

    // read the register
    let mut value = read_register(device_address, address)?;

    // isolate the specific bit
    value = get_bit(value, gpio);

    return Ok(value);
}

// function to generalize attribute setting functions
fn set_attribute(device_address: u16, gpio: u8, address: u8, value: u8) -> Result<()> {
    // validate the gpio number
    if !is_gpio_valid(gpio) {
        return Err(errors::ApplicationError::new("invalid GPIO"));
    }

    // perform the register write to set the attribute
    set_register_bit(device_address, address, gpio, value & 0x01)
}

// find the value of a single bit
fn get_bit(value: u8, bit_num: u8) -> u8 {
    // isolate the specific bit
    return (value >> bit_num) & 0x1;
}

// change the value of a single bit
fn set_bit(mut reg_value: u8, bit_num: u8, value: u8) -> u8 {
    reg_value ^= ((-(value as i8) as u8) ^ reg_value) & (1 << bit_num);
    return reg_value as u8;
}

// functions to set attribute for all gpios
pub fn set_all_direction(device_address: u16, input: u8) -> Result<()> {
    set_register(device_address, MCP23008_REG_IODIR, input)
}

// functions to read attribute for specific gpio
pub fn get_direction(device_address: u16, gpio: u8) -> Result<u8> {
    get_attribute(device_address, gpio, MCP23008_REG_IODIR)
}

pub fn set_all_pullup(device_address: u16, pull_up: u8) -> Result<()> {
    set_register(device_address, MCP23008_REG_GPPU, pull_up)
}

pub fn set_all_gpio(device_address: u16, value: u8) -> Result<()> {
    set_register(device_address, MCP23008_REG_GPIO, value)
}

pub fn get_input_polarity(device_address: u16, gpio: u8) -> Result<u8> {
    get_attribute(device_address, gpio, MCP23008_REG_IPOL)
}

// overwrite all values in register
fn set_register(device_address: u16, address: u8, value: u8) -> Result<()> {
    i2c::write(
        MCP23008_I2C_DEVICE_NUM,
        MCP23008_I2C_DEVICE_ADDR + device_address,
        address,
        value,
    )?;
    // if (status == EXIT_FAILURE) {
    // 	onionPrint(ONION_SEVERITY_FATAL, "mcp-driver:: writing value 0x%02x to addr 0x%02x failed\n", val, addr);
    // }

    return Ok(());
}

// set only a single bit in a register
fn set_register_bit(device_address: u16, address: u8, bit_num: u8, bit_value: u8) -> Result<()> {
    // read the value
    let mut value = read_register(device_address, address)?;

    // inject the bit value
    value = set_bit(value, bit_num, bit_value);

    // write the value
    set_register(device_address, address, value)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_set_bit() {
        let mut reg_value = 1;
        let bit_num = 1;
        let bit_value = 1;

        reg_value = set_bit(reg_value, bit_num, bit_value);

        assert_eq!(3, reg_value);
    }
}
