use crate::i2c;
use crate::Result;

const DEVICE_ADDRESS: u16 = 0x3C;
const DEVICE_NUM: u8 = i2c::DEFAULT_ADAPTER;

pub const WIDTH: u8 = 128;
pub const HEIGHT: u8 = 64;
const PAGES: u8 = 8;
const CHAR_LENGTH: u8 = 6;
// const NUM_CHARS: u8 =	96;

const CHAR_COLUMNS: u8 = 21;
pub const CHAR_ROWS: u8 = 8;
const CHAR_COLUMN_PIXELS: u8 = CHAR_COLUMNS * CHAR_LENGTH;

const CONTRAST_MIN: u8 = 0;
const CONTRAST_MAX: u8 = 255;

// Registers
const REG_DATA: u8 = 0x40;
const REG_COMMAND: u8 = 0x80;

// Addresses
const ADDR_BASE_PAGE_START: u8 = 0xB0;

const ASCII_TABLE: [[u8; CHAR_LENGTH as usize]; 96] = [
    [0x00, 0x00, 0x00, 0x00, 0x00, 0x00], // SPACE
    [0x00, 0x00, 0x4F, 0x00, 0x00, 0x00], // !
    [0x00, 0x07, 0x00, 0x07, 0x00, 0x00], // "
    [0x14, 0x7F, 0x14, 0x7F, 0x14, 0x00], // #
    [0x24, 0x2A, 0x7F, 0x2A, 0x12, 0x00], // $
    [0x23, 0x13, 0x08, 0x64, 0x62, 0x00], // %
    [0x36, 0x49, 0x55, 0x22, 0x50, 0x00], // &
    [0x00, 0x05, 0x03, 0x00, 0x00, 0x00], // '
    [0x00, 0x1C, 0x22, 0x41, 0x00, 0x00], // (
    [0x00, 0x41, 0x22, 0x1C, 0x00, 0x00], // )
    [0x14, 0x08, 0x3E, 0x08, 0x14, 0x00], // *
    [0x08, 0x08, 0x3E, 0x08, 0x08, 0x00], // +
    [0x00, 0x50, 0x30, 0x00, 0x00, 0x00], // ,
    [0x08, 0x08, 0x08, 0x08, 0x08, 0x00], // -
    [0x00, 0x60, 0x60, 0x00, 0x00, 0x00], // .
    [0x20, 0x10, 0x08, 0x04, 0x02, 0x00], // /
    [0x3E, 0x51, 0x49, 0x45, 0x3E, 0x00], // 0
    [0x00, 0x42, 0x7F, 0x40, 0x00, 0x00], // 1
    [0x42, 0x61, 0x51, 0x49, 0x46, 0x00], // 2
    [0x21, 0x41, 0x45, 0x4B, 0x31, 0x00], // 3
    [0x18, 0x14, 0x12, 0x7F, 0x10, 0x00], // 4
    [0x27, 0x45, 0x45, 0x45, 0x39, 0x00], // 5
    [0x3C, 0x4A, 0x49, 0x49, 0x30, 0x00], // 6
    [0x01, 0x71, 0x09, 0x05, 0x03, 0x00], // 7
    [0x36, 0x49, 0x49, 0x49, 0x36, 0x00], // 8
    [0x06, 0x49, 0x49, 0x29, 0x1E, 0x00], // 9
    [0x36, 0x36, 0x00, 0x00, 0x00, 0x00], // :
    [0x56, 0x36, 0x00, 0x00, 0x00, 0x00], // ;
    [0x08, 0x14, 0x22, 0x41, 0x00, 0x00], // <
    [0x14, 0x14, 0x14, 0x14, 0x14, 0x00], // =
    [0x00, 0x41, 0x22, 0x14, 0x08, 0x00], // >
    [0x02, 0x01, 0x51, 0x09, 0x06, 0x00], // ?
    [0x30, 0x49, 0x79, 0x41, 0x3E, 0x00], // @
    [0x7E, 0x11, 0x11, 0x11, 0x7E, 0x00], // A
    [0x7F, 0x49, 0x49, 0x49, 0x36, 0x00], // B
    [0x3E, 0x41, 0x41, 0x41, 0x22, 0x00], // C
    [0x7F, 0x41, 0x41, 0x22, 0x1C, 0x00], // D
    [0x7F, 0x49, 0x49, 0x49, 0x41, 0x00], // E
    [0x7F, 0x09, 0x09, 0x09, 0x01, 0x00], // F
    [0x3E, 0x41, 0x49, 0x49, 0x7A, 0x00], // G
    [0x7F, 0x08, 0x08, 0x08, 0x7F, 0x00], // H
    [0x00, 0x41, 0x7F, 0x41, 0x00, 0x00], // I
    [0x20, 0x40, 0x41, 0x3F, 0x01, 0x00], // J
    [0x7F, 0x08, 0x14, 0x22, 0x41, 0x00], // K
    [0x7F, 0x40, 0x40, 0x40, 0x40, 0x00], // L
    [0x7F, 0x02, 0x0C, 0x02, 0x7F, 0x00], // M
    [0x7F, 0x04, 0x08, 0x10, 0x7F, 0x00], // N
    [0x3E, 0x41, 0x41, 0x41, 0x3E, 0x00], // O
    [0x7F, 0x09, 0x09, 0x09, 0x06, 0x00], // P
    [0x3E, 0x41, 0x51, 0x21, 0x5E, 0x00], // Q
    [0x7F, 0x09, 0x19, 0x29, 0x46, 0x00], // R
    [0x46, 0x49, 0x49, 0x49, 0x31, 0x00], // S
    [0x01, 0x01, 0x7F, 0x01, 0x01, 0x00], // T
    [0x3F, 0x40, 0x40, 0x40, 0x3F, 0x00], // U
    [0x1F, 0x20, 0x40, 0x20, 0x1F, 0x00], // V
    [0x3F, 0x40, 0x30, 0x40, 0x3F, 0x00], // W
    [0x63, 0x14, 0x08, 0x14, 0x63, 0x00], // X
    [0x07, 0x08, 0x70, 0x08, 0x07, 0x00], // Y
    [0x61, 0x51, 0x49, 0x45, 0x43, 0x00], // Z
    [0x00, 0x7F, 0x41, 0x41, 0x00, 0x00], // [
    [0x02, 0x04, 0x08, 0x10, 0x20, 0x00], // backslash
    [0x00, 0x41, 0x41, 0x7F, 0x00, 0x00], // ]
    [0x04, 0x02, 0x01, 0x02, 0x04, 0x00], // ^
    [0x40, 0x40, 0x40, 0x40, 0x40, 0x00], // _
    [0x00, 0x01, 0x02, 0x04, 0x00, 0x00], // `
    [0x20, 0x54, 0x54, 0x54, 0x78, 0x00], // a
    [0x7F, 0x50, 0x48, 0x48, 0x30, 0x00], // b
    [0x38, 0x44, 0x44, 0x44, 0x20, 0x00], // c
    [0x38, 0x44, 0x44, 0x48, 0x7F, 0x00], // d
    [0x38, 0x54, 0x54, 0x54, 0x18, 0x00], // e
    [0x08, 0x7E, 0x09, 0x01, 0x02, 0x00], // f
    [0x0C, 0x52, 0x52, 0x52, 0x3E, 0x00], // g
    [0x7F, 0x08, 0x04, 0x04, 0x78, 0x00], // h
    [0x00, 0x44, 0x7D, 0x40, 0x00, 0x00], // i
    [0x20, 0x40, 0x44, 0x3D, 0x00, 0x00], // j
    [0x7F, 0x10, 0x28, 0x44, 0x00, 0x00], // k
    [0x00, 0x41, 0x7F, 0x40, 0x00, 0x00], // l
    [0x78, 0x04, 0x78, 0x04, 0x78, 0x00], // m
    [0x7C, 0x08, 0x04, 0x04, 0x78, 0x00], // n
    [0x38, 0x44, 0x44, 0x44, 0x38, 0x00], // o
    [0x7C, 0x14, 0x14, 0x14, 0x08, 0x00], // p
    [0x08, 0x14, 0x14, 0x18, 0x7C, 0x00], // q
    [0x7C, 0x08, 0x04, 0x04, 0x08, 0x00], // r
    [0x48, 0x54, 0x54, 0x54, 0x20, 0x00], // s
    [0x04, 0x3F, 0x44, 0x40, 0x20, 0x00], // t
    [0x3C, 0x40, 0x40, 0x20, 0x7C, 0x00], // u
    [0x1C, 0x20, 0x40, 0x20, 0x1C, 0x00], // v
    [0x3C, 0x40, 0x30, 0x40, 0x3C, 0x00], // w
    [0x44, 0x28, 0x10, 0x28, 0x44, 0x00], // x
    [0x0C, 0x50, 0x50, 0x50, 0x3C, 0x00], // y
    [0x44, 0x64, 0x54, 0x4C, 0x44, 0x00], // z
    [0x00, 0x08, 0x36, 0x41, 0x00, 0x00], // {
    [0x00, 0x00, 0x7F, 0x00, 0x00, 0x00], // |
    [0x00, 0x41, 0x36, 0x08, 0x00, 0x00], // }
    [0x0C, 0x02, 0x0C, 0x10, 0x0C, 0x00], // ~
    [0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
];

// Command Constants
#[derive(Debug)]
pub enum Command {
    SetContrast = 0x81,
    DisplayAllOnResume = 0xA4,
    DisplayAllOn = 0xA5,
    NormalDisplay = 0xA6,
    InvertDisplay = 0xA7,
    DisplayOff = 0xAE,
    DisplayOn = 0xAF,
    SetDisplayOffset = 0xD3,
    SetComPins = 0xDA,
    SetVcomDetect = 0xDB,
    SetDisplayClockDiv = 0xD5,
    SetPrecharge = 0xD9,
    SetMultiplex = 0xA8,
    SetLowColumn = 0x00,
    SetHighColumn = 0x10,
    SetStartLine = 0x40,
    MemoryMode = 0x20,
    ColumnAddr = 0x21,
    PageAddr = 0x22,
    ComScanInc = 0xC0,
    ComScanDec = 0xC8,
    SegRemap = 0xA0,
    ChargePump = 0x8D,
    ActivateScroll = 0x2F,
    SetVerticalScrollArea = 0xA3,
    RightHorizontalScroll = 0x26,
    LeftHorizontalScroll = 0x27,
    VerticalAndRightHorizontalScroll = 0x29,
    VerticalAndLeftHorizontalScroll = 0x2A,
    DeactivateScroll = 0x2E,
    // ExternalVCC 		= 0x01,
    // SwitchCapVCC 	= 0x02,
}

const EXTERNAL_VCC: u8 = 0x01;
const SWITCH_CAP_VCC: u8 = 0x02;

const DEF_CONTRAST_EXTERNAL_VCC: u8 = 0x9f;
const DEF_CONTRAST_SWITCH_CAP_VCC: u8 = 0xcf;

#[derive(Clone, Copy)]
pub enum MemoryMode {
    HorizontalAddrMode = 0x00,
    VerticalAddrMode = 0x01,
    PageAddrMode = 0x02,
}

const BUFFER_SIZE: usize = WIDTH as usize * PAGES as usize;

pub enum ScrollSpeed {
    F5 = 0x00,
    F64 = 0x01,
    F128 = 0x02,
    F256 = 0x03,
    F3 = 0x04,
    F4 = 0x05,
    F25 = 0x06,
    F2 = 0x07,
}

pub struct Display {
    vcc_state: u8,
    memory_mode: MemoryMode,
    buffer: [u8; BUFFER_SIZE],
    cursor: u8,
    cursor_in_row: u8,
    columns_set_for_text: u8,
}

impl Display {
    pub fn new() -> Display {
        Display {
            vcc_state: 0,
            memory_mode: MemoryMode::HorizontalAddrMode,
            buffer: [0; BUFFER_SIZE],
            cursor: 0,
            cursor_in_row: 0,
            columns_set_for_text: 0,
        }
    }

    // Initialize the OLED Expansion
    pub fn driver_init(&mut self) -> Result<()> {
        log::info!("Initializing display");

        self.buffer = [0; BUFFER_SIZE]; // Initialize the buffer
        self.cursor = 0; // Initialize the cursor
        self.cursor_in_row = 0; // Initialize the row cursor

        // set defaults
        self.vcc_state = SWITCH_CAP_VCC;

        // Initialize the screen for 128x64
        send_command(Command::DisplayOff)?;
        // usleep(4500);

        send_command(Command::SetDisplayClockDiv)?;
        set_command(0x80)?; // The suggested ratio 0x80
        send_command(Command::SetMultiplex)?;
        set_command(0x3F)?;
        send_command(Command::SetDisplayOffset)?;
        set_command(0x00)?; // no offset
        set_command(Command::SetStartLine as u8 | 0x00)?; // Set start line to line #0
        send_command(Command::ChargePump)?;

        if self.vcc_state == EXTERNAL_VCC {
            set_command(0x10)?;
        } else {
            set_command(0x14)?;
        }

        self.set_memory_mode(MemoryMode::HorizontalAddrMode)?;

        set_command(Command::SegRemap as u8 | 0x01)?;
        send_command(Command::ComScanDec)?;
        send_command(Command::SetComPins)?;
        set_command(0x12)?;
        send_command(Command::SetContrast)?;

        if self.vcc_state == EXTERNAL_VCC {
            set_command(DEF_CONTRAST_EXTERNAL_VCC)?;
        } else {
            set_command(DEF_CONTRAST_SWITCH_CAP_VCC)?;
        }

        send_command(Command::SetPrecharge)?;
        if self.vcc_state == EXTERNAL_VCC {
            set_command(0x22)?;
        } else {
            set_command(0xF1)?;
        }

        send_command(Command::SetVcomDetect)?;
        set_command(0x40)?;
        send_command(Command::DisplayAllOnResume)?;
        send_command(Command::NormalDisplay)?;

        send_command(Command::SegRemap)?;
        send_command(Command::ComScanInc)?;

        send_command(Command::DisplayOn)?;
        // usleep(4500);

        // clear the display
        set_display_mode(DisplayMode::Normal)?;
        clear()
    }

    // set the horizontal addressing for text (fit 6-pixel wide characters onto 128 pixel line)
    pub fn set_text_columns(&mut self) -> Result<()> {
        // set the column addressing for text mode
        set_column_addressing(0, CHAR_COLUMN_PIXELS - 1)?;
        self.columns_set_for_text = 1;
        Ok(())
    }

    // set the horizontal addressing for images (rows go from pixel 0 to 127, full width)
    pub fn set_image_columns(&mut self) -> Result<()> {
        // set the column addressing to full width
        set_column_addressing(0, WIDTH - 1)?;
        self.columns_set_for_text = 0;
        Ok(())
    }

    // Write a byte directly to the OLED display (at the OLED cursor's current position)
    pub fn write_byte(&self, byte: u8) -> Result<()> {
        send_data(byte)
    }

    // Write a string message directly to the display
    pub fn write(&mut self, msg: &str) -> Result<()> {
        log::info!("Writing '{}' to display", msg);

        // set addressing mode to page
        //oledSetMemoryMode(OLED_EXP_MEM_PAGE_ADDR_MODE);	// want automatic newlines enabled

        // set column addressing to fit 126 characters that are 6 pixels wide
        if self.columns_set_for_text == 0 {
            self.set_text_columns()?;
        }

        // write each character
        for ch in msg.chars() {
            // check for newline character
            if ch == '\n' {
                // move the cursor to the next row
                for _i in self.cursor_in_row..CHAR_COLUMNS {
                    self.write_char(' ')?;
                }

            // increment past this newline character (skip next index)
            // idx += 1;
            } else {
                self.write_char(ch)?;
            }
        }

        // reset the column addressing
        set_image_columns()
    }

    // Write a character directly to the OLED display (at the OLED cursor's current position)
    pub fn write_char(&mut self, c: char) -> Result<()> {
        let char_index = c as usize - 32;

        // ensure character is in the table
        if char_index < ASCII_TABLE.len() {
            // write the data for the character
            for index in 0..CHAR_LENGTH as usize {
                let data = ASCII_TABLE[char_index][index];
                send_data(data)?;
            }

            log::debug!("\twriting '{}' to column {}", c, self.cursor_in_row);

            // increment row cursor
            if self.cursor_in_row == CHAR_COLUMNS - 1 {
                self.cursor_in_row = 0;
            } else {
                self.cursor_in_row += 1;
            }
        }

        Ok(())
    }

    // set the screen to normal brightness or dimmed brightness
    pub fn set_dim(&self, dim: bool) -> Result<()> {
        // set the brightness based on the dimness setting
        let mut brightness;

        if dim {
            // dim
            brightness = CONTRAST_MIN;
            log::info!("Dimming display");
        } else {
            // normal
            brightness = DEF_CONTRAST_SWITCH_CAP_VCC;
            if self.vcc_state == EXTERNAL_VCC {
                brightness = DEF_CONTRAST_EXTERNAL_VCC;
            }

            log::info!("Setting normal display brightness");
        };

        // send the command
        set_brightness(brightness)
    }

    // set the display's memory mode
    pub fn set_memory_mode(&mut self, mode: MemoryMode) -> Result<()> {
        // send the command
        send_command(Command::MemoryMode)?;
        set_command(mode as u8)?;

        // store the memory mode
        self.memory_mode = mode;

        Ok(())
    }

    // Write a buffer directly to the display
    pub fn draw(&mut self, buffer: &[u8]) -> Result<()> {
        //int 	swap;	// removing bit flip

        log::info!("Writing buffer data to display");

        // set the column addressing for the full width
        set_image_columns()?;

        // set addressing mode to horizontal (automatic newline at the end of each line)
        self.set_memory_mode(MemoryMode::HorizontalAddrMode)?;

        // write each byte
        for byte in buffer {
            //let swap = reverse_byte(byte);
            // send_data(swap)?;
            send_data(*byte)?;
        }

        Ok(())
    }
}

fn send_command(command: Command) -> Result<()> {
    i2c::write(DEVICE_NUM, DEVICE_ADDRESS, REG_COMMAND, command as u8).map(|_| ())
}

fn set_command(value: u8) -> Result<()> {
    i2c::write(DEVICE_NUM, DEVICE_ADDRESS, REG_COMMAND, value).map(|_| ())
}

// Send data byte to OLED Expansion
fn send_data(data: u8) -> Result<()> {
    i2c::write(DEVICE_NUM, DEVICE_ADDRESS, REG_DATA, data).map(|_| ())
}

// Check if i2c transmissions to the display work
pub fn check_init() -> Result<()> {
    i2c::read_byte(DEVICE_NUM, DEVICE_ADDRESS, REG_COMMAND).map(|_| ())
}

// set the display to on or off
pub fn set_display_power(power_on: bool) -> Result<()> {
    if power_on {
        log::info!("Setting display to ON");
        send_command(Command::DisplayOn)
    } else {
        log::info!("Setting display to OFF");
        send_command(Command::DisplayOff)
    }
}

pub enum DisplayMode {
    Normal,
    Inverted,
}

// set the display to normal or inverted mode
pub fn set_display_mode(mode: DisplayMode) -> Result<()> {
    match mode {
        DisplayMode::Normal => {
            log::debug!("Setting display mode to normal");
            send_command(Command::NormalDisplay)
        }
        DisplayMode::Inverted => {
            log::debug!("Setting display mode to inverted");
            send_command(Command::InvertDisplay)
        }
    }
}

// Clear the OLED screen
pub fn clear() -> Result<()> {
    log::debug!("Clearing display");

    // set the column addressing for the full width
    set_image_columns()?;

    // display off
    send_command(Command::DisplayOff)?;

    // write a blank space to each character
    for char_row in 0..CHAR_ROWS {
        set_cursor(char_row, 0)?;

        for _pixel_col in 0..WIDTH {
            send_data(0x00)?;
        }
    }

    // display on
    send_command(Command::DisplayOn)?;

    // reset the cursor to (0, 0)
    set_cursor(0, 0)
}

// set the OLED's cursor (according to character rows and columns)
pub fn set_cursor(row: u8, column: u8) -> Result<()> {
    log::debug!("Setting cursor to ({}, {})", row, column);

    // check the inputs
    if row >= CHAR_ROWS {
        panic!("Attempting to set cursor to invalid row {}", row);
    }

    if column >= CHAR_COLUMNS {
        panic!("Attempting to set cursor to invalid column {}", column);
    }

    //// set the cursor
    // set page address
    set_command(ADDR_BASE_PAGE_START + row)?;

    // set column lower address
    let lower_address = Command::SetLowColumn as u8 + (CHAR_LENGTH * column & 0x0F);
    set_command(lower_address)?;

    // set column higher address
    let higher_address = Command::SetHighColumn as u8 + ((CHAR_LENGTH * column >> 4) & 0x0F);
    set_command(higher_address)
}

// set the OLED's cursor (according to character rows and dislay pixels)
pub fn set_cursor_by_pixel(row: u8, pixel: u8) -> Result<()> {
    log::debug!("Setting cursor to row {}, pixel {})", row, pixel);

    // check the inputs
    if row >= CHAR_ROWS {
        panic!("Attempting to set cursor to invalid row '{}'", row);
    }

    if pixel >= WIDTH {
        panic!("Attempting to set cursor to invalid pixel '{}'", pixel);
    }

    //// set the cursor
    // set page address
    set_command(ADDR_BASE_PAGE_START + row)?;

    // set pixel lower address
    set_command(Command::SetLowColumn as u8 + (pixel & 0x0F))?;

    // set pixel higher address
    set_command(Command::SetHighColumn as u8 + ((pixel >> 4) & 0x0F))
}

// set the horizontal addressing for images (rows go from pixel 0 to 127, full width)
pub fn set_image_columns() -> Result<()> {
    // set the column addressing to full width
    set_column_addressing(0, WIDTH - 1)
}

// set the horizontal addressing
pub fn set_column_addressing(start_pixel: u8, end_pixel: u8) -> Result<()> {
    // check the inputs
    if start_pixel >= WIDTH || start_pixel >= end_pixel {
        panic!(
            "Invalid start pixel {} for column address setup",
            start_pixel
        );
    }

    if end_pixel >= WIDTH {
        panic!("Invalid end pixel {} for column address setup", end_pixel);
    }

    // set column addressing
    send_command(Command::ColumnAddr)?;
    set_command(start_pixel)?;
    set_command(end_pixel)
}

// set the display's brightness
fn set_brightness(brightness: u8) -> Result<()> {
    // clamp the brightness to the lower and upper limits
    let mut brightness_value = brightness;
    if brightness < CONTRAST_MIN {
        brightness_value = CONTRAST_MIN;
    }

    if brightness > CONTRAST_MAX {
        brightness_value = CONTRAST_MAX;
    }

    // send the command
    log::debug!(
        "Setting display brightness to {}/{}",
        brightness_value,
        CONTRAST_MAX
    );

    send_command(Command::SetContrast)?;
    set_command(brightness_value)
}

//// scrolling functions ////
// horizontal scrolling
//	direction 	scrolling
//	0 			left
//	1 			right
pub fn scroll(direction: u8, speed: ScrollSpeed, start_page: u8, stop_page: u8) -> Result<()> {
    let scroll_mode = if direction == 1 {
        log::info!("Enabling horizontal scrolling to the right");
        Command::RightHorizontalScroll
    } else {
        log::info!("Enabling horizontal scrolling to the left");
        Command::LeftHorizontalScroll
    };

    // send the commands
    send_command(scroll_mode)?;
    set_command(0x00)?; // dummy byte
    set_command(start_page)?; // start page addr (0 - 7)
    set_command(speed as u8)?; // time interval between frames
    set_command(stop_page)?; // end page addr (must be greater than start)
    set_command(0x00)?; // dummy byte (must be 0x00)
    set_command(0xff)?; // dummy byte (must be 0xff)

    send_command(Command::ActivateScroll)
}

// diagonal scrolling
//	direction 	scrolling
//	0 			left
//	1 			right
pub fn scroll_diagonal(
    direction: u8,
    speed: ScrollSpeed,
    fixed_rows: u8,
    scroll_rows: u8,
    vertical_offset: u8,
    start_page: u8,
    stop_page: u8,
) -> Result<()> {
    let scroll_mode = if direction == 1 {
        log::info!("Enabling diagonal scrolling to the right");

        Command::VerticalAndRightHorizontalScroll
    } else {
        log::info!("Enabling diagonal scrolling to the left");

        Command::VerticalAndLeftHorizontalScroll
    };

    //// send the commands
    // setup the vertical scrolling
    send_command(Command::SetVerticalScrollArea)?;
    set_command(fixed_rows)?; // number of fixed rows
    set_command(scroll_rows)?; // number of rows in scroll area

    // setup the horizontal scrolling
    send_command(scroll_mode)?;
    set_command(0x00)?; // dummy byte
    set_command(start_page)?; // start page addr (0 - 7)
    set_command(speed as u8)?; // time interval between frames
    set_command(stop_page)?; // end page addr (must be greater than start)
    set_command(vertical_offset)?; // number of rows to scroll by

    send_command(Command::ActivateScroll)
}

// disable scrolling
pub fn scroll_stop() -> Result<()> {
    log::info!("Disabling scrolling");

    send_command(Command::DeactivateScroll)
}
